# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2024-01-08
### Fixed
- unified samples and instruments retrieval from bags
### Added
- getters for key related attributes in presets
### Changed
- test framework from nose to unittest

## [0.9.0] - 2017-02-08
### Changed
- added support for midi key influence to pitch generator
- added edge case where no instrument is linked from preset (was crashing during parse)

## [0.8.0] - 2016-02-02
### Added
- tags and comment related methods in RenoiseInstrument

[0.9.0]: https://gitlab.com/zeograd/sf2utils/-/tags/0.9.0
[1.0.0]: https://gitlab.com/zeograd/sf2utils/-/tags/v1.0.0
